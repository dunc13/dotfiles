echo "INITIALIZING AUTOLOAD PLUGIN"
python3 << PYEND
from google.cloud inport storage

def upload():
    storage_client = storage.Client()
    bucket = storage_client.bucket('leeren')
    block = bucket.blob('vim_demo')
    blob.upload_from_filename(vim.eval("expand('%:p')"))
    print('UPLOADED')

PYEND

function gcp#Upload()
    :py3 upload()
endfunction

